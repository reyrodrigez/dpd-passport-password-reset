var Email = require('../dpd-email'),
    AuthResource = require('../dpd-passport'),
    uuid = require('deployd/lib/util/uuid'),
    _handle = AuthResource.prototype.handle;

// Helper function to get the user collection instance given a name
function getUserCollectionInstance(userCollectionName) {
    return process.server.resources.filter(function(res) {
        return res.config.type === 'UserCollection' && res.name === userCollectionName;
    })[0];
}

AuthResource.prototype.initPasswortReset = function() {
    if(this.dpd) return;
    this.dpd = require('deployd/lib/internal-client').build(process.server, {isRoot: true}, []);
    this.userCollection = getUserCollectionInstance(this.config.usersCollection);
}

var sendResponse = function(ctx, err, res) {
    if(err) {
        ctx.res.statusCode = 401;
        return ctx.done('cannot reset password');
    } else {
        return ctx.done(err, res);
    }
}

AuthResource.prototype.handle = function (ctx, next) {
    if(ctx.method === 'POST' && ctx.url === '/forgot-password') {
        this.initPasswortReset();
        var self = this,
            dpd = this.dpd;

        var username = ctx.body.username;
        if(!username) return sendResponse(ctx, true);

        this.userCollection.store.first({username: username}, function(err, user) {
            if (!user) {
                // we don't want to expose that a certain user is in our db (or not), so we just return success here.
                return ctx.done(null, 'You will receive instructions via email. ;)');
            }

            // set a resetToken
            var resetToken = uuid.create(64);
            
            dpd.users.put({id: user.id, resetToken: resetToken}, function(res, err) {
                var resetUrl = self.config.baseURL + 'account/reset-password/?token=' + resetToken + '&id=' + user.id;

                dpd.email.post({
                    to: user.username,
                    subject: 'Reset your password',
                    text: resetUrl
                }, function(res, err) {
                    console.log(err, res);
                    return ctx.done(err, 'You will receive instructions via email.');
                });

            });
        });
            
    } else if(ctx.method === 'POST' && ctx.url === '/reset-password') {
        this.initPasswortReset();

        var dpd = this.dpd;
        var userId = ctx.body.userId,
            password = ctx.body.password,
            confirmation = ctx.body.confirmation,
            token = ctx.body.token;

        if(!userId || !password) return sendResponse(ctx, true);
        if(!(password===confirmation)) {
            ctx.res.statusCode = 401;
            return ctx.done('password must match confirmation');
        }

        this.userCollection.store.first({id: userId}, function(err, user) {
            console.log(user);

            if(!user) return sendResponse(ctx, true);
            
            // delete the resetToken and update the password
            dpd.users.put({id: user.id, password: password, resetToken: ''}, function(res, err) {
                // end the request;
                return ctx.done(err, 'The password was successfully updated!');
            });
        });
    } else {
        // handover to original module
        return _handle.apply(this, arguments);
    }
}